console.log(`hello world`)

console.log("Hello");


//ES6 Updates

//1. Exponent Operator
//before
const firstNum = Math.pow(8, 2);


//after
const secondNum = 8 ** 2;


//2. Template Literals
//Allows to write strings without using the concatenation operator (+)
//Uses backticks(``) ${expression}

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`)



//3. Array Destructuring
//Allows us to unpack elements in arrays into distinct variables
//Syntax:
	//let const [variableNameA, variableNameB, variableNameC] = array;


const fullName = ["Juan", "Dela", "Cruz"];

//Pre-Array Destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

//Array Destructuring
const [firstName, middleName, lastName] = fullName;


console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)
//Helps with code readability

//4. Object Destructuring
//Allows to unpack properties of objects into distinct variables
//Syntax:
	//let/const {propertyName, propertyName, propertyName} = object;
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`)

//Object Destructing

const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
//Shortens the syntax for accessing properties from objects


function getFullName({ givenName, maidenName, familyName}) {
	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`)
}

getFullName(person);

//5. Arrow Function

//traditional functions

function printFullName(firstName, middleName, lastName) {
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName("John", "Dela", "Cruz");


//arrow function
/*
Syntax:
	let/const variableName = (parameterA, parameterB) => {
		statement
	}

*/
let printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName1("Jane", "D", "Smith");


const students = ["John", "Jane", "Jasmine"];

//Arrow functions with loops
//Pre-arrow function
students.forEach(function(student) {
	console.log(`${student} is a student.`)
})


//Arrow Function
students.forEach(student => {
	console.log(`${student} is a student.`)
})
//we can omit the paranthesis in arrow function if we only have one paramater.



//Implicit Return Statement
//There are instances when you can omit the "return" statement

//Pre - Arrow Function

// const add = (x, y) => {
// 	return x + y
// }

// let total = add(1, 2);
// console.log(total);


//Arrow Function
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);


//Default Function Argument Value
//Provide a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good morning, ${name}!`
}

console.log(greet());


//Class-Based Object Blueprints
//Allows creation of objects using classes as blueprints
//Creating a class
	//constructor is a special method of a class for creating an object
	//this keyword that refers to the properties of an object created from the class

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}


}

const myCar = new Car();

console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);



//TERNARY OPERATOR
// (condition) ? truthy : falsy

//Activity Solution:

//OMG!!! MAY SOLUTION NA PALA?!!!!! kaya pala the same HAHAHAHA

//3.
let num = 5;
let getCube = num ** 3;
//4.
console.log(`The cube of ${num} is ${getCube}`);
//5.
//6.
const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

//7.
//8
const animal = {
	name: "Lolong",
	species: 'saltwater crocodile',
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)


let numbers = [1, 2, 3, 4, 5];

//implicit return statement

numbers.forEach(number => console.log(number));


let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber)


//class
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Brownie", 6, "Aspin");
console.log(myDog);











